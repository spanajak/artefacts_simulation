using UnityEngine;

public class TestControlls : MonoBehaviour
{
    private TestEvents events;
    private RecordEvents record;

    private void Awake()
    {
        events = GetComponent<TestEvents>();
        record = GetComponent<RecordEvents>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.S))
        {
            events.RaiseOnStart();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            events.RaiseOnEnd();
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            events.RaiseOnJudge();
        }
        */

        if (Input.GetKeyDown("[1]"))
        {
            record.RaiseOnViewJudge("1");
            events.RaiseOnJudge();
        }

        if (Input.GetKeyDown("[2]"))
        {
            record.RaiseOnViewJudge("2");
            events.RaiseOnJudge();
        }

        if (Input.GetKeyDown("[3]"))
        {
            record.RaiseOnViewJudge("3");
            events.RaiseOnJudge();
        }

        if (Input.GetKeyDown("[4]"))
        {
            record.RaiseOnViewJudge("4");
            events.RaiseOnJudge();
        }

        if (Input.GetKeyDown("[5]"))
        {
            record.RaiseOnViewJudge("5");
            events.RaiseOnJudge();
        }

        if (Input.GetKeyDown("[5]"))
        {
            record.RaiseOnViewJudge("Unwell");
            events.RaiseOnJudge();
        }
    }
}
