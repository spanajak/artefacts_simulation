using UnityEngine;

public class SphereManager : MonoBehaviour
{
    private GameObject backgroundSphere;
    [SerializeField]
    private GameObject[] layerSpheres;
    private GameObject fadeSphere;

    private void Awake()
    {
        backgroundSphere = GameObject.FindGameObjectWithTag("Background");
        layerSpheres = GameObject.FindGameObjectsWithTag("Layer");
        fadeSphere = GameObject.FindGameObjectWithTag("Fade");
    }

    public GameObject GetFade()
    {
        return fadeSphere;
    }

    public GameObject GetBackground()
    {
        return backgroundSphere;
    }

    public GameObject[] GetLayers(int layerCount)
    {
        if (layerCount > layerSpheres.Length)
        {
            Debug.LogError("Missing layer: not enough layers");
            return null;
        }

        GameObject[] spheres = new GameObject[layerCount];

        for (int i = 0; i < layerCount; i++)
        {
            spheres[i] = layerSpheres[i];
        }

        return spheres;
    }
}
