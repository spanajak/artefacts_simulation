using UnityEngine;

public class MainHandler : MonoBehaviour
{
    public Transform VrCamera;
    private BackgroundHandler backgroundHandler;
    private LayerHandler[] layerHandlers;

    private void Awake()
    {
        backgroundHandler = GetComponentInChildren<BackgroundHandler>();
        layerHandlers = GetComponentsInChildren<LayerHandler>();
    }

    public Transform GetCamera()
    {
        return VrCamera;
    }

    public BackgroundHandler GetBackground()
    {
        return backgroundHandler;
    }

    public LayerHandler[] GetLayers(int layerCount)
    {
        if (layerCount > layerHandlers.Length)
        {
            Debug.LogError("Missing layerHandlers: not enough layers");
            return null;
        }

        LayerHandler[] lHandlers = new LayerHandler[layerCount];

        for (int i = 0; i < layerCount; i++)
        {
            lHandlers[i] = layerHandlers[i];
        }

        return lHandlers;
    }
}


public enum NoiseType
{
    Static,
    Dynamic,
    HeadsetStatic
}
