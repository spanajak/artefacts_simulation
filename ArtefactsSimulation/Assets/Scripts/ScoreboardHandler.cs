using UnityEngine;

public class ScoreboardHandler : MonoBehaviour
{
    public GameObject scoreboard;
    public GameObject[] hands;

    private Transform cameraTransform;
    public Vector3 offset;

    private TestEvents events;

    private void Awake()
    {
        events = GetComponentInParent<TestEvents>();

        events.OnStart += OnStart;
        events.OnJudge += OnJudge;

        events.OnEnd += DelayedShow;
    }

    private void Start()
    {
        cameraTransform = GetComponent<MainHandler>().GetCamera();
    }

    private void OnDisable()
    {
        events.OnStart -= OnStart;
        events.OnJudge -= OnJudge;

        events.OnEnd -= DelayedShow;
    }

    private void OnJudge()
    {
        scoreboard.SetActive(false);

        foreach (GameObject hand in hands)
        {
            hand.SetActive(false);
        }
    }

    public void ShowScoreboard()
    {
        scoreboard.SetActive(true);
        Invoke("DelayedShow",0.5f);
    }

    private void DelayedShow()
    {
        foreach (GameObject hand in hands)
        {
            hand.SetActive(true);
        }
    }

    private void OnStart()
    {
        scoreboard.transform.position = cameraTransform.position + offset;
        scoreboard.transform.rotation = Quaternion.Euler(0f, cameraTransform.rotation.eulerAngles.y, 0f);

        foreach (GameObject hand in hands)
        {
            hand.SetActive(false);
        }
    }
}
