using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ViewInfo
{
    public string viewName;
    public BackgroundTexture backgroundTexture;
    public bool active;
}

