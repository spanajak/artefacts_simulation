using UnityEngine;

[System.Serializable]
public class FrameDropInfo : ViewInfo
{ 
    public int frameRepetition = 1;
    public bool dynamicFrameDrop;
    public float intervalStatic;
    public Vector2 intervalRange;
}
