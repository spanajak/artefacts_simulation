[System.Serializable]
public class FlickerInfo : ViewInfo
{
    public float minExposure = -1f;
    public float maxExposure = 1f;
    public int frameCount = 1;
}
