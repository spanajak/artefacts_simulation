using System.Collections.Generic;
using UnityEngine;

public class FlickerView : BaseView
{
    private float minExposure = -1f;
    private float maxExposure = 1f;
    private int frameCnt = 1;
    private int frameCounter = 0;

    private bool max = true;
    private PostProcessController ppc;

    public FlickerInfo[] flickerViews;

    public override List<int> Get()
    {
        List<int> activeElements = new List<int>();

        for (int i = 0; i < flickerViews.Length; i++)
        {
            if (flickerViews[i].active)
            {
                activeElements.Add(i);
            }
        }

        return activeElements;
    }

    public override void Set(int index)
    {
        if (index >= flickerViews.Length)
        {
            Debug.LogError("Base View out of elements");
        }
        else
        {
            viewName = flickerViews[index].viewName;
            backgroundTexture = flickerViews[index].backgroundTexture;
            active = flickerViews[index].active;

            minExposure = flickerViews[index].minExposure;
            maxExposure= flickerViews[index].maxExposure;
            frameCnt = flickerViews[index].frameCount;
        }
    }

    public override void Activate()
    {
        ppc = GetComponent<PostProcessController>();
        frameCounter = 0;

        base.Activate();
    }

    public override void Do()
    {
        /*
        float flickerValue = Mathf.Sin(Time.time * Mathf.PI *2f *  frequency);

        // Adjust exposure within the specified range
        float targetExposure = Mathf.Lerp(minExposure, maxExposure, (flickerValue + 1) / 2);
        currentExposure = Mathf.Lerp(currentExposure, targetExposure, Time.deltaTime);

        ppc.SetBrightness(currentExposure);
        */
        
        frameCounter++;

        if (frameCounter == frameCnt)
        {
            frameCounter = 0;

            if (max)
            {
                ppc.SetBrightness(minExposure);
            }
            else {
                ppc.SetBrightness(maxExposure);
            }

            max = !max;
        }
        
    }

    public override void Deactivate()
    {
        ppc.SetBrightness(0);

        base.Deactivate();
    }
}
