using UnityEngine;
using System.Collections.Generic;

public class GazeFollowView : BaseView
{
    private MainHandler handler;
    private LayerHandler layerHandler;
    private GazeFollowLayer gazeFollowLayer;

    private float rotationSpeed = 10f;
    private Vector3 offset;

    private Texture texture;

    public GazeFollowInfo[] gazeFollowViews;

    public override List<int> Get()
    {
        List<int> activeElements = new List<int>();

        for (int i = 0; i < gazeFollowViews.Length; i++)
        {
            if (gazeFollowViews[i].active)
            {
                activeElements.Add(i);
            }
        }

        return activeElements;
    }

    public override void Set(int index)
    {
        if (index >= gazeFollowViews.Length)
        {
            Debug.LogError("Base View out of elements");
        }
        else
        {
            viewName = gazeFollowViews[index].viewName;
            backgroundTexture = gazeFollowViews[index].backgroundTexture;
            active = gazeFollowViews[index].active;

            rotationSpeed = gazeFollowViews[index].rotationSpeed;
            offset = gazeFollowViews[index].offset;
            texture = gazeFollowViews[index].texture;
        }
    }

    public override void Activate()
    {
        handler = GetComponent<MainHandler>();
        layerHandler = handler.GetLayers(1)[0];
        gazeFollowLayer = layerHandler.GetGazeFollow();
        layerHandler.GetMatHandler().SetTexture(texture);
        
        layerHandler.SetOffset(offset);
        layerHandler.Activate();
        gazeFollowLayer.Activate();
        gazeFollowLayer.SetRotationSpeed(rotationSpeed);

        base.Activate();
    }

    public override void Deactivate()
    {
        layerHandler.SetOffset(Vector3.zero);
        layerHandler.Deactivate();
        gazeFollowLayer.Deactivate();
        layerHandler.GetMatHandler().SetDefaultTexture();

        base.Deactivate();
    }

    public override void LateDo()
    {
        gazeFollowLayer.LateDo();
    }
}