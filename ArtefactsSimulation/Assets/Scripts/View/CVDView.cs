using UnityEngine;
using System.Collections.Generic;

public class CVDView : BaseView
{
    private PostProcessController ppc;
    private CVD cvd;

    private int Lshift = 0;
    private int Mshift = 0;
    private int Sshift = 0;

    public CVDInfo[] CVDViews;

    public override List<int> Get()
    {
        List<int> activeElements = new List<int>();

        for (int i = 0; i < CVDViews.Length; i++)
        {
            if (CVDViews[i].active)
            {
                activeElements.Add(i);
            }
        }

        return activeElements;
    }

    public override void Set(int index)
    {
        if (index >= CVDViews.Length)
        {
            Debug.LogError("Base View out of elements");
        }
        else
        {
            viewName = CVDViews[index].viewName;
            backgroundTexture = CVDViews[index].backgroundTexture;
            active = CVDViews[index].active;

            Lshift = CVDViews[index].Lshift;
            Mshift = CVDViews[index].Mshift;
            Sshift = CVDViews[index].Sshift;
        }
    }

    private void Awake()
    {
        ppc = GetComponent<PostProcessController>();
        cvd = GetComponent<CVD>();
    }

    public override void Activate()
    {
        float[,] cm= cvd.GetRGBMatrix(Lshift, Mshift, Sshift);
        Vector3 red = new Vector3(cm[0,0], cm[0, 1], cm[0, 2]);
        Vector3 green = new Vector3(cm[1, 0], cm[1, 1], cm[1, 2]);
        Vector3 blue = new Vector3(cm[2, 0], cm[2, 1], cm[2, 2]);
        ppc.SetColorMixer(red, green, blue);
    }

    public override void Deactivate()
    {
        ppc.ResetColorMixer();
    }
}
