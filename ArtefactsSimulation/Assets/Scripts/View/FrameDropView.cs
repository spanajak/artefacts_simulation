using UnityEngine;
using System.Collections.Generic;

public class FrameDropView : BaseView
{
    private FrameDropController fdc;
    private int frameRepetition = 1;
    private bool dynamicFrameDrop;
    private float intervalStatic;
    private Vector2 intervalRange;

    public FrameDropInfo[] frameDropViews;

    public override List<int> Get()
    {
        List<int> activeElements = new List<int>();

        for (int i = 0; i < frameDropViews.Length; i++)
        {
            if (frameDropViews[i].active)
            {
                activeElements.Add(i);
            }
        }

        return activeElements;
    }

    public override void Set(int index)
    {
        if (index >= frameDropViews.Length)
        {
            Debug.LogError("Base View out of elements");
        }
        else
        {
            viewName = frameDropViews[index].viewName;
            backgroundTexture = frameDropViews[index].backgroundTexture;
            active = frameDropViews[index].active;

            frameRepetition = frameDropViews[index].frameRepetition;
            dynamicFrameDrop = frameDropViews[index].dynamicFrameDrop;
            intervalStatic = frameDropViews[index].intervalStatic;
            intervalRange = frameDropViews[index].intervalRange;
        }
    }

    public override void Activate()
    {
        fdc = GetComponent<FrameDropController>();
        fdc.SetFrameRepetition(frameRepetition);

        if (dynamicFrameDrop)
        {
            fdc.SetInterval(intervalRange);
        }
        else {
            fdc.SetInterval(intervalStatic);
        }

        base.Activate();
    }

    public override void LateDo()
    {
        fdc.LateDo();
    }

    public override void Deactivate()
    {
        fdc.Deactivate();

        base.Deactivate();
    }
}
