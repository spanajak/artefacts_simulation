using UnityEngine;
using System.Collections.Generic;

public class NoiseView : BaseView
{
    private Texture noiseTexture;
    private NoiseType noiseType;

    private MainHandler handler;
    private LayerHandler layerHandler;
    private Transform VRCam;
    private Transform pivot;

    public NoiseInfo[] noiseViews;

    public override List<int> Get()
    {
        List<int> activeElements = new List<int>();

        for (int i = 0; i < noiseViews.Length; i++)
        {
            if (noiseViews[i].active)
            {
                activeElements.Add(i);
            }
        }

        return activeElements;
    }

    public override void Set(int index)
    {
        if (index >= noiseViews.Length)
        {
            Debug.LogError("Base View out of elements");
        }
        else
        {
            viewName = noiseViews[index].viewName;
            backgroundTexture = noiseViews[index].backgroundTexture;
            active = noiseViews[index].active;

            noiseTexture = noiseViews[index].noiseTexture;
            noiseType = noiseViews[index].noiseType;
        }
    }

    public override void Activate()
    {
        handler = GetComponent<MainHandler>();
        layerHandler = handler.GetLayers(1)[0];
        layerHandler.GetMatHandler().SetTexture(noiseTexture);
        pivot = layerHandler.GetPivot();
        layerHandler.Activate();

        VRCam = handler.GetCamera();

        base.Activate();
    }

    public override void Deactivate()
    {
        layerHandler.SetOffset(Vector3.zero);
        layerHandler.Deactivate();
        layerHandler.GetMatHandler().SetDefaultTexture();

        pivot.position = Vector3.zero;
        pivot.rotation = Quaternion.Euler(0f,0f,0f);

        layerHandler.SetOffset(Vector3.zero);

        base.Deactivate();
    }

    public override void LateDo()
    {
        if (noiseType == NoiseType.Dynamic)
        {
            layerHandler.SetOffset(new Vector3(Random.Range(0f, 360f), Random.Range(0f, 360f), Random.Range(0f, 360f)));
        }
        else if (noiseType == NoiseType.HeadsetStatic)
        {
            pivot.position = VRCam.position;
            pivot.rotation = VRCam.rotation;
        }
    }
}

