using UnityEngine;
using System.Collections.Generic;

public class BaseView : MonoBehaviour
{
    [HideInInspector]
    public string viewName;
    [HideInInspector]
    public BackgroundTexture backgroundTexture;
    [HideInInspector]
    public bool active;
    
    private MaterialHandler matH;

    public string GetName()
    {
        return viewName;
    }

    public virtual List<int> Get() {
        return null;
    }

    public virtual void Set(int index) { }

    public virtual void Do() { }
    public virtual void LateDo() { }
    public virtual void Activate() {

        if (backgroundTexture == BackgroundTexture.None)
        {
            Debug.LogWarning("Missing texture");
        }
        else {
            matH = GetComponent<MainHandler>().GetBackground().GetMatHandler();
            matH.SetCube(backgroundTexture);
        }

    }
    public virtual void Deactivate() {

        if (backgroundTexture != BackgroundTexture.None)
        {
            matH.SetDefaultCube();
        }
    }
}

