using UnityEngine;
using System.Collections.Generic;

public class ColorMatrixView : BaseView
{
    private Vector3 red;
    private Vector3 green;
    private Vector3 blue;

    private PostProcessController ppc;

    public ColorMatrixInfo[] colorMatrixViews;

    public override List<int> Get()
    {
        List<int> activeElements = new List<int>();

        for (int i = 0; i < colorMatrixViews.Length; i++)
        {
            if (colorMatrixViews[i].active)
            {
                activeElements.Add(i);
            }
        }

        return activeElements;
    }

    public override void Set(int index)
    {
        if (index >= colorMatrixViews.Length)
        {
            Debug.LogError("Base View out of elements");
        }
        else
        {
            viewName = colorMatrixViews[index].viewName;
            backgroundTexture = colorMatrixViews[index].backgroundTexture;
            active = colorMatrixViews[index].active;

            red = colorMatrixViews[index].red;
            green = colorMatrixViews[index].green;
            blue = colorMatrixViews[index].blue;
        }
    }

    private void Awake()
    {
        ppc = GetComponent<PostProcessController>();    
    }

    public override void Activate()
    {
        ppc.SetColorMixer(red, green, blue);

        base.Activate();
    }

    public override void Deactivate()
    {
        ppc.ResetColorMixer();

        base.Deactivate();
    }
}
