using UnityEngine;

[System.Serializable]
public class ColorMatrixInfo : ViewInfo
{
    public Vector3 red;
    public Vector3 green;
    public Vector3 blue;
}
