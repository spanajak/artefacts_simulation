using UnityEngine;
using System.Collections.Generic;

public class EmptyView : BaseView
{
    public ViewInfo[] emptyViews;

    public override List<int> Get()
    {
        List<int> activeElements = new List<int>();

        for (int i = 0; i < emptyViews.Length; i++)
        {
            if (emptyViews[i].active)
            {
                activeElements.Add(i);
            }
        }

        return activeElements;
    }

    public override void Set(int index)
    {
        if (index >= emptyViews.Length)
        {
            Debug.LogError("Base View out of elements");
        }
        else
        {
            viewName = emptyViews[index].viewName;
            backgroundTexture = emptyViews[index].backgroundTexture;
            active = emptyViews[index].active;
        }
    }
}
