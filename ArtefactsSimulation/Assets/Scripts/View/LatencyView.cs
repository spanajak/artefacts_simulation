using UnityEngine;
using System.Collections.Generic;

public class LatencyView : BaseView
{
    private LatencyController latencyController;
    private int frameLatency;

    public LatencyInfo[] latencyViews;

    public override List<int> Get()
    {
        List<int> activeElements = new List<int>();

        for (int i = 0; i < latencyViews.Length; i++)
        {
            if (latencyViews[i].active)
            {
                activeElements.Add(i);
            }
        }

        return activeElements;
    }

    public override void Set(int index)
    {
        if (index >= latencyViews.Length)
        {
            Debug.LogError("Base View out of elements");
        }
        else
        {
            viewName = latencyViews[index].viewName;
            backgroundTexture = latencyViews[index].backgroundTexture;
            active = latencyViews[index].active;

            frameLatency = latencyViews[index].frameLatency;
        }
    }

    public override void Activate()
    {
        latencyController = GetComponent<LatencyController>();
        latencyController.SetLatency(frameLatency);

        base.Activate();
    }

    public override void LateDo()
    {
        latencyController.LateDo();
    }

    public override void Deactivate()
    {
        latencyController.Deactivate();

        base.Deactivate();
    }


}
