using UnityEngine;
[System.Serializable]
public class NoiseInfo : ViewInfo
{
    public Texture noiseTexture;
    public NoiseType noiseType;
}
