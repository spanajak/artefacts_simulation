using UnityEngine;
[System.Serializable]
public class GazeFollowInfo : ViewInfo
{
    public float rotationSpeed = 10f;
    public Vector3 offset;
    public Texture texture;
}
