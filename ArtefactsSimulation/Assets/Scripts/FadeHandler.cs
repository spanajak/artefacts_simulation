using UnityEngine;
using System.Collections;

public class FadeHandler : MonoBehaviour
{
    public delegate void ReturnFunction();

    public float fadingDuration = 1f;

    public Material fadeMaterial;

    private void Awake()
    {
        FadeToTransparent(null);
    }

    public void SetVisible()
    {
        Color currentColor = fadeMaterial.color;
        currentColor.a = 1f;
        fadeMaterial.color = currentColor;
        Debug.Log("setting to visible");
    }

    public void SetInvisible()
    {
        Color currentColor = fadeMaterial.color;
        currentColor.a = 0f;
        fadeMaterial.color = currentColor;
        Debug.Log("setting to invisible");
    }

    public void FadeToVisible(ReturnFunction functionDelegate)
    {
        StartCoroutine(MakeVisibleCoroutine(functionDelegate));
    }

    public void FadeToTransparent(ReturnFunction functionDelegate)
    {
        StartCoroutine(MakeTransparentCoroutine(functionDelegate));    
    }

    IEnumerator MakeVisibleCoroutine(ReturnFunction functionDelegate)
    {
        //Material layerMaterial = layerSphere.GetComponent<Renderer>().material;
        //Color layerColor = layerMaterial.color;

        float startTime = Time.time;

        Color currentColor = fadeMaterial.color;

        float currentTransparency = currentColor.a;

        while (currentTransparency < 1f)
        {
            float elapsedTime = Time.time - startTime;

            currentTransparency = Mathf.Lerp(0f, 1f, elapsedTime / fadingDuration);

            currentColor.a = currentTransparency;
            //layerColor.a = 1 - currentTransparency;

            //layerMaterial.color = layerColor;
            
            fadeMaterial.color = currentColor;

            yield return null;
        }

        if (functionDelegate != null) functionDelegate();
    }
    
    IEnumerator MakeTransparentCoroutine(ReturnFunction functionDelegate)
    {
       // Material layerMaterial = layerSphere.GetComponent<Renderer>().material;
        //Color layerColor = layerMaterial.color;

        float startTime = Time.time;

        Color currentColor = fadeMaterial.color;

        float currentTransparency = currentColor.a;

        while (currentTransparency > 0f)
        {
            float elapsedTime = Time.time - startTime;

            currentTransparency = Mathf.Lerp(1f, 0f, elapsedTime / fadingDuration);

            currentColor.a = currentTransparency;
            //layerColor.a = 1 - currentTransparency;

            //layerMaterial.color = layerColor;
            fadeMaterial.color = currentColor;

            yield return null;
        }

        if (functionDelegate != null) functionDelegate();
    }
    
}
