using UnityEngine;

public class ScorePoint : MonoBehaviour
{
    public float maxSize = 1.5f;
    public float rateOfChange = 0.001f;

    private float maxRelativeSize;
    private float currentSize;
    private float minRelativeSize;
    private bool isScaling = false;

    private TestEvents events;
    private RecordEvents record;

    private void Awake()
    {
        currentSize = transform.localScale.x;
        maxRelativeSize = currentSize * maxSize;
        minRelativeSize = currentSize;

        events = GetComponentInParent<TestEvents>();
        record = GetComponentInParent<RecordEvents>();
    }

    private void OnTriggerEnter(Collider other)
    {
        isScaling = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isScaling = false;
    }

    private void Update()
    {
        if (isScaling)
        {
            currentSize += rateOfChange*Time.deltaTime;
            if (currentSize > maxRelativeSize)
            {
                currentSize = minRelativeSize;

                record.RaiseOnViewJudge(this.name);
                events.RaiseOnJudge();

                isScaling = false;

            }
        }
        else {
            currentSize -= rateOfChange * Time.deltaTime;
            if (currentSize < minRelativeSize)
            {
                currentSize = minRelativeSize;
            }
        }

        transform.localScale = currentSize * Vector3.one;
    }
}
