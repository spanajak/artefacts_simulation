using UnityEngine;
using System.Collections.Generic;

public class LatencyController : MonoBehaviour
{
    private Transform VrCamera;
    private int frameLatency = 1;
    List<Vector3> rotationList;
    private Transform backgroundPivot;
    private Transform background;
    private Transform pivot2;
    private Transform pivot3;

    private MainHandler handler;
  
    void Awake()
    {
        rotationList = new List<Vector3>();
        handler = GetComponent<MainHandler>();
    }

    private void Start()
    {
        VrCamera = handler.GetCamera();
        backgroundPivot = handler.GetBackground().GetPivot();
        background = handler.GetBackground().GetBackGround();
        pivot2 = handler.GetBackground().GetPivot2();
        pivot3 = handler.GetBackground().GetPivot3();
    }

    public void LateDo()
    {
        
        Vector3 newRotation = VrCamera.localEulerAngles;
        backgroundPivot.position = VrCamera.position;
        rotationList.Add(newRotation);

        if (frameLatency +1 <= rotationList.Count)
        {
            background.position = VrCamera.position;
            backgroundPivot.rotation = VrCamera.localRotation;
            background.localRotation = Quaternion.Euler(0f, -rotationList[0].y, 0f);
            pivot2.localRotation = Quaternion.Euler(-rotationList[0].x, 0f, 0f);
            pivot3.localRotation = Quaternion.Euler(0f, 0f, -rotationList[0].z);
            rotationList.RemoveAt(0);
        }
    }

    public void SetLatency(int newLatency)
    {
        frameLatency = newLatency;
        rotationList.Clear();
    }

    public void Deactivate()
    {

        backgroundPivot.position = Vector3.zero;
        backgroundPivot.localRotation = Quaternion.Euler(0f, 0f, 0f);

        background.localRotation = Quaternion.Euler(0f, 0f, 0f);
        pivot2.localRotation = Quaternion.Euler(0f, 0f, 0f);
        pivot3.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }

}
