using UnityEngine;
using System.IO;
using System;

public class RecordManager : MonoBehaviour
{
    public string subjectName;
    private TestEvents events;
    private RecordEvents record;

    public bool testing;
    private string testName;

    public string defaultDirectoryPath = @"E:\Users\student\Desktop\ArtefactsResults";
    private string currentDirectoryPath;

    private string pathHead;
    private string pathEye;
    private string pathAll;
    private string pathScore;
    private string pathTime;

    private string viewName;

    private void Awake()
    {
        events = GetComponent<TestEvents>();
        record = GetComponent<RecordEvents>();

        events.OnStart += OnStart;
        record.OnViewJudge += OnViewJudge;
        record.OnNewViewName += OnNewViewName;
        record.OnRecordAll += OnRecordAll;
        record.OnRecordNull += OnRecordNull;
        record.OnRecordEye += OnRecordEye;
        record.OnRecordHead += OnRecordHead;
        record.OnRecordTime += OnRecordTime;

        if (Directory.Exists(defaultDirectoryPath))
        {
            Debug.Log("directory exists");
        }
        else {
            Directory.CreateDirectory(defaultDirectoryPath);
        }
    }

    private void OnDisable()
    {
        events.OnStart -= OnStart;
        record.OnViewJudge -= OnViewJudge;
        record.OnNewViewName -= OnNewViewName;
        record.OnRecordAll -= OnRecordAll;
        record.OnRecordNull -= OnRecordNull;
        record.OnRecordEye -= OnRecordEye;
        record.OnRecordHead -= OnRecordHead;
        record.OnRecordTime -= OnRecordTime;
    }

    public void SetName(string newName)
    {
        subjectName = newName;
    }

    public void SetTesting(bool newTesting)
    {
        testing = newTesting;
    }
    

    private void OnStart()
    {
        // Set subject folder
        if (testing)
        {
            testName = "Test";
        }
        else {
            testName = subjectName;
        }

        currentDirectoryPath = defaultDirectoryPath + '\\' + testName;

        // Creating subject folder
        try
        {
            Directory.CreateDirectory(currentDirectoryPath);
            Debug.Log("Folder created successfully.");
        }
        catch (Exception ex)
        {
            Debug.Log("An error occurred: " + ex.Message);
        }

        // Creating sub folders
        pathHead = currentDirectoryPath + "\\Head";
        pathEye = currentDirectoryPath + "\\Eye";
        pathAll = currentDirectoryPath + "\\All";
        pathTime = currentDirectoryPath + "\\Time";
        pathScore = currentDirectoryPath + "\\Score.txt";
        

        try
        {
            Directory.CreateDirectory(pathHead);
            Directory.CreateDirectory(pathEye);
            Directory.CreateDirectory(pathAll);
            Directory.CreateDirectory(pathTime);

            Debug.Log("Folder created successfully.");
        }
        catch (Exception ex)
        {
            Debug.Log("An error occurred: " + ex.Message);
        }

        // Create score.txt file
        File.WriteAllText(pathScore, "");

        Debug.Log("paths created succesfully");
    }

    private void OnNewViewName(string newName)
    {
        Debug.Log(pathScore);
        viewName = "//" + newName + ".txt";

        // Add line to Score file
        File.AppendAllText(pathScore, newName + " ");

        // Create files for eye tracking
        File.WriteAllText(pathHead + viewName, "");
        File.WriteAllText(pathEye + viewName, "");
        File.WriteAllText(pathAll  + viewName, "");
        File.WriteAllText(pathTime + viewName, "");
    }

    private void OnRecordNull()
    {
        File.AppendAllText(pathAll + viewName, Environment.NewLine);
        File.AppendAllText(pathEye + viewName, Environment.NewLine);
    }

    private void OnRecordAll(Vector3 recordInfo)
    {
        File.AppendAllText(pathAll + viewName, recordInfo.x.ToString() + " " + recordInfo.y.ToString() + " " + recordInfo.z.ToString() + " " + Environment.NewLine);
    }

    private void OnRecordHead(Vector3 recordInfo)
    {
        File.AppendAllText(pathHead + viewName, recordInfo.x.ToString() + " " + recordInfo.y.ToString() + " " + recordInfo.z.ToString() + " " + Environment.NewLine);
    }

    private void OnRecordEye(Vector2 recordInfo)
    {
        File.AppendAllText(pathEye + viewName, recordInfo.x.ToString() + " " + recordInfo.y.ToString() + " "  + Environment.NewLine);
    }

    private void OnRecordTime(float recordInfo)
    {
        File.AppendAllText(pathTime + viewName, recordInfo.ToString() + Environment.NewLine);
    }

    private void OnViewJudge(string score)
    {
        File.AppendAllText(pathScore, score + Environment.NewLine);
    }
}
