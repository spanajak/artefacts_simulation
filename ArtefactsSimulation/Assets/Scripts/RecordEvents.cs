using UnityEngine;
using System;

public class RecordEvents : MonoBehaviour
{
    public event Action<string> OnNewViewName;
    public event Action<string> OnViewJudge;

    public event Action<Vector3> OnRecordHead;
    public event Action<Vector2> OnRecordEye;
    public event Action<Vector3> OnRecordAll;
    public event Action<float> OnRecordTime;
    public event Action OnRecordNull;

    public void RaiseOnNewViewName(string newName)
    {
        OnNewViewName?.Invoke(newName);
    }

    public void RaiseOnViewJudge(string score)
    {
        OnViewJudge?.Invoke(score);
    }

    public void RaiseOnRecordHead(Vector3 recordInfo)
    {
        OnRecordHead?.Invoke(recordInfo);
    }

    public void RaiseOnRecordEye(Vector2 recordInfo)
    {
        OnRecordEye?.Invoke(recordInfo);
    }

    public void RaiseOnRecordAll(Vector3 recordInfo)
    {
        OnRecordAll?.Invoke(recordInfo);
    }

    public void RaiseOnRecordTime(float recordInfo)
    {
        OnRecordTime?.Invoke(recordInfo);
    }

    public void RaiseOnRecordNull()
    {
        OnRecordNull?.Invoke();
    }
}
