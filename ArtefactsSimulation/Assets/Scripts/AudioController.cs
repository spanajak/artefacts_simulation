using UnityEngine;

public class AudioController : MonoBehaviour
{
    private TestEvents events;
    private AudioManager audioManager;

    private void Awake()
    {
        events = GetComponent<TestEvents>();
        audioManager = GetComponent<AudioManager>();

        events.OnViewStart += OnViewStart;
        events.OnJudge += OnJudge;
        events.OnViewEnd += OnViewEnd;
    }

    private void OnDisable()
    {
        events.OnViewStart -= OnViewStart;
        events.OnJudge -= OnJudge;
        events.OnViewEnd -= OnViewEnd;
    }

    private void OnViewStart()
    {
        audioManager.Play("ViewStart");
    }

    private void OnViewEnd()
    {
        audioManager.Play("ViewEnd");
    }

    private void OnJudge()
    {
        audioManager.Play("Judge");
    }

}
