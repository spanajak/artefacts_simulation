using UnityEngine;
using System;

public class TestEvents : MonoBehaviour
{
    public event Action OnStart;
    public event Action OnEnd;

    public event Action OnJudge;

    public event Action OnViewStart;
    public event Action OnViewEnd;

    public event Action<Vector3> OnGazeDirection;


    public void RaiseOnStart()
    {
        OnStart?.Invoke();
    }

    public void RaiseOnEnd()
    {
        Debug.Log("Raising onEnd");
        OnEnd?.Invoke();
    }
    public void RaiseOnJudge()
    {
        OnJudge?.Invoke();
    }

    public void RaiseOnViewStart()
    {
        OnViewStart?.Invoke();
    }

    public void RaiseOnViewEnd()
    {
        OnViewEnd?.Invoke();
    }

    public void RaiseOnGazeDirection(Vector3 direction)
    {
        OnGazeDirection?.Invoke(direction);
    }

}
