using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public ViewManager VM;
    public Text progressText;
    public Text viewName;

    public GameObject HideButton;
    public GameObject ShowButton;

    private TestEvents events;

    private void Start()
    {
        events = VM.gameObject.GetComponent<TestEvents>();

        events.OnViewStart += UpdateInfo;
        events.OnEnd += UpdateInfo;

        SetText(VM.currentViewIdx + 1, VM.viewsCnt);
        SetViewName(VM.currentActiveView);
    }

    private void OnDisable()
    {
        events.OnViewStart -= UpdateInfo;
        events.OnEnd -= UpdateInfo;
    }

    private void UpdateInfo()
    {
        SetText(VM.currentViewIdx + 1, VM.viewsCnt);
        SetViewName(VM.currentActiveView);
    }

    public void SetText(int current, int max)
    {
        progressText.text = current.ToString() + " / "+  max.ToString();
    }

    public void SetViewName(ActiveView currentView)
    {
        if (currentView == null)
        {
            viewName.text = "None";
            return;
        }
        viewName.text = currentView.baseView.GetName() ;
    }

    public void SwitchToHide()
    {
        ShowButton.SetActive(false);
        HideButton.SetActive(true);
    }

    public void SwitchToShow()
    {
        HideButton.SetActive(false);
        ShowButton.SetActive(true);
    }

    public void StartTest()
    {
        events.RaiseOnStart();
    }
}

