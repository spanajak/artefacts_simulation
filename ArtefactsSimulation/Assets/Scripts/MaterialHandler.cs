using UnityEngine;

public class MaterialHandler : MonoBehaviour
{
    private Material mat;
    private Texture defaultTexture;
    private Texture defaultCube;

    public Texture flowers;
    public Texture biscayne;
    public Texture hokkaido;

    private void Awake()
    {
        mat = GetComponent<Renderer>().material;
        defaultTexture = mat.GetTexture("_MainTex");
        defaultCube = mat.GetTexture("_Tex");
    }

    public void SetCube(Texture texture)
    {
        mat.SetTexture("_Tex", texture);
    }

    public void SetCube(BackgroundTexture BT)
    {
        if (BT == BackgroundTexture.Flowers)
        {
            mat.SetTexture("_Tex", flowers);
        }
        else if (BT == BackgroundTexture.Hokkaido)
        {
            mat.SetTexture("_Tex", hokkaido);
        }
        else if (BT == BackgroundTexture.Biscayne)
        {
            mat.SetTexture("_Tex", biscayne);
        }
    }

    public void SetDefaultCube()
    {
        mat.SetTexture("_Tex", defaultCube);
    }

    public void SetTexture(Texture texture)
    {
        mat.SetTexture("_MainTex", texture);
        mat.SetTexture("_EmissionMap", texture);
    }
    

    public void SetDefaultTexture()
    {
        mat.SetTexture("_MainTex", defaultTexture);
        mat.SetTexture("_EmissionMap", defaultTexture);
    }
}

public enum BackgroundTexture
{
    Flowers,
    Biscayne,
    Hokkaido,
    None
}
