using UnityEngine;

public class GazeFollowLayer : MonoBehaviour
{
    private Transform pivot;
    private Transform vrCamera;
    private TestEvents events;
    private float rotationSpeed;

    private LayerHandler layerHandler;

    private void Start()
    {
        events = GetComponentInParent<TestEvents>();

        layerHandler = GetComponent<LayerHandler>();
        pivot = layerHandler.GetPivot();
        vrCamera = layerHandler.GetCamera();
    }

    public void SetRotationSpeed(float newRotationSpeed)
    {
        rotationSpeed = newRotationSpeed;
    }

    public void LateDo()
    {
        pivot.position = vrCamera.position;
        pivot.rotation = vrCamera.rotation;
    }

    private void OnGaze(Vector3 gaze)
    {
        Quaternion targetRotation = Quaternion.LookRotation(gaze);
        targetRotation = Quaternion.Euler(0f, gaze.x, gaze.y);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    public void Activate()
    {
        events.OnGazeDirection += OnGaze;
    }

    public void Deactivate()
    {
        events.OnGazeDirection -= OnGaze;

        pivot.position = Vector3.zero;
        pivot.rotation = Quaternion.Euler(0f, 0f, 0f);
        transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }
}
