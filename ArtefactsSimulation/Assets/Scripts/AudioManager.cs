using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    Dictionary<string, AudioSource> audioDictionary;

    private void Awake()
    {
        audioDictionary = new Dictionary<string, AudioSource>();

        foreach (Sound sound in sounds)
        {
            AudioSource audioSource = this.gameObject.AddComponent<AudioSource>();
            audioSource.clip = sound.audioClip;

            audioDictionary[sound.name] = audioSource;
        }
    }

    public void Play(string audioName)
    {
        if (audioDictionary.ContainsKey(audioName))
        {
            audioDictionary[audioName].Play();
        }
    }
}
