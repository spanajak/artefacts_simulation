using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameCounter : MonoBehaviour
{
    public float avgTimeDelta;

    private TestEvents events;

    private float timeSum = 0f;
    private int frameCnt = 0;
    private bool testing = false;

    private float avgSum = 0f;
    private int viewCounts = 0;


    private void Awake()
    {
        Application.targetFrameRate = 30;

        events = GetComponent<TestEvents>();

        events.OnViewStart += OnViewStart;
        events.OnViewEnd += OnViewEnd;

        events.OnStart += OnStart;
        events.OnEnd += OnEnd;
    }

    private void OnDisable()
    {
        events.OnViewStart -= OnViewStart;
        events.OnViewEnd -= OnViewEnd;

        events.OnStart -= OnStart;
        events.OnEnd -= OnEnd;
    }

    private void Update()
    {
        if (testing)
        {
            timeSum += Time.deltaTime;
            frameCnt++;

        }
    }

    private void OnStart()
    {
        float avgSum = 0f;
        int viewCounts = 0;
    }

    private void OnEnd()
    {
        Debug.Log("Average delay between frames is : " + (avgSum / viewCounts).ToString());
    }

    private void OnViewStart()
    {
        testing = true;
        timeSum = 0f;
        frameCnt = 0;
    }

    private void OnViewEnd()
    {
        avgTimeDelta = timeSum / frameCnt;

        avgSum += avgTimeDelta;
        viewCounts++;
    }
    
}
