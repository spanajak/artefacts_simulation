using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessController : MonoBehaviour
{
    public PostProcessVolume processVolume;
    private ColorGrading colorLayer;

    private Vector3 defaultRed = new Vector3(100f, 0f, 0f);
    private Vector3 defaultGreen = new Vector3(0f, 100f, 0f);
    private Vector3 defaultBlue = new Vector3(0f, 0f, 100f);

    private void Awake()
    {
        processVolume.profile.TryGetSettings<ColorGrading>(out colorLayer);
    }

    public void SetBrightness(float newBrightness)
    {
        colorLayer.postExposure.value = newBrightness;
    }

    public void SetColorMixer(Vector3 outRed, Vector3 outGreen, Vector3 outBlue)
    {
        if (outRed == null) outRed = defaultRed;
        if (outGreen == null) outGreen = defaultGreen;
        if (outBlue == null) outBlue = defaultBlue;

        // Vector3.x - red; .y -greem; .z - blue

        colorLayer.mixerRedOutRedIn.value = outRed.x;
        colorLayer.mixerRedOutGreenIn.value = outRed.y;
        colorLayer.mixerRedOutBlueIn.value = outRed.z;

        colorLayer.mixerGreenOutRedIn.value = outGreen.x;
        colorLayer.mixerGreenOutGreenIn.value = outGreen.y;
        colorLayer.mixerGreenOutBlueIn.value = outGreen.z;

        colorLayer.mixerBlueOutRedIn.value = outBlue.x;
        colorLayer.mixerBlueOutGreenIn.value = outBlue.y;
        colorLayer.mixerBlueOutBlueIn.value = outBlue.z;
    }

    public void ResetColorMixer()
    {
        SetColorMixer(defaultRed, defaultGreen, defaultBlue);
    }

}
