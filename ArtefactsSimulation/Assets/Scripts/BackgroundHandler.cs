using UnityEngine;

public class BackgroundHandler : MonoBehaviour
{
    private MaterialHandler matHandler;
    public Transform pivot;
    public Transform pivot2;
    public Transform pivot3;

    private void Awake()
    {
        matHandler = GetComponent<MaterialHandler>();
    }

    public Transform GetPivot()
    {
        return pivot;
    }

    public Transform GetPivot2()
    {
        return pivot2;
    }

    public Transform GetPivot3()
    {
        return pivot3;
    }

    public Transform GetBackGround()
    {
        return transform;
    }
    
    public MaterialHandler GetMatHandler()
    {
        return matHandler;
    }
}
