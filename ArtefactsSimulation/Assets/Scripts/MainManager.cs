using UnityEngine;
using System.Collections;

public class MainManager : MonoBehaviour
{
    public float timeInterval = 10f;

    private Coroutine timerCoroutine;
    private ViewManager viewManager;
    private FadeHandler fadeHandler;
    private ScoreboardHandler scoreboardHandler;

    private TestEvents events;

    private void Awake()
    {
        viewManager = GetComponent<ViewManager>();
        fadeHandler = GetComponent<FadeHandler>();
        scoreboardHandler = GetComponent<ScoreboardHandler>();
        events = GetComponent<TestEvents>();

        events.OnStart += OnStart;
        events.OnJudge += OnJudge;

        events.OnViewEnd += OnViewEnd;
    }

    private void OnDisable()
    {
        events.OnStart -= OnStart;
        events.OnJudge -= OnJudge;

        events.OnViewEnd -= OnViewEnd;
    }

    private void OnStart()
    {
        Debug.Log("Staring the test");
        fadeHandler.SetInvisible();
        fadeHandler.FadeToVisible(PrepareView);
    }

    public void PrepareView()
    {
        if (viewManager.NextView())
        {
            fadeHandler.FadeToTransparent(StartView);
        }
        else {
            fadeHandler.FadeToTransparent(null);
            events.RaiseOnEnd();
        }
    }

    public void StartView()
    {
        timerCoroutine = StartCoroutine(TimerCoroutine());
        events.RaiseOnViewStart();
        // start record eye movements
    }

    public void OnViewEnd()
    {
        StopTimer();
        //events.RaiseOnViewEnd();
        fadeHandler.FadeToVisible(ShowScoreboard);
    }

    public void ShowScoreboard()
    {
        // shows scoreboard
        scoreboardHandler.ShowScoreboard();
        viewManager.DeactivateView();
    }

    public void OnJudge()
    {
        PrepareView();
    }

    IEnumerator TimerCoroutine()
    {
        yield return new WaitForSeconds(timeInterval);
        events.RaiseOnViewEnd();
    }

    private void OnEnd()
    {
        StopTimer();
    }
    private void StopTimer()
    {
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
        }
    }
}
