using UnityEngine;
using ViveSR.anipal.Eye;
using System.Runtime.InteropServices;
using System.Collections;

public class EyeTracker : MonoBehaviour
{
    public float skipTime;
    public float samplePeriod;

    private static EyeData eyeData = new EyeData();
    private static bool eye_callback_registered = false;

    private bool recordEye;
    private bool inView = false;

    private float elapsedTime = 0f;

    private float closedEyesTime;

    private Coroutine eyeTrackerCoroutine;

    private TestEvents events;
    private RecordEvents record;
    public Vector3 offset;

    [Header("VIVE EYE TRACKER")]
    [SerializeField]
    public GameObject VRcam;
    public Transform head;
    public Transform eye;
    public Transform gaze;

    private void Awake()
    {
        events = GetComponent<TestEvents>();
        record = GetComponent<RecordEvents>();

        events.OnViewStart += OnViewStart;
        events.OnViewEnd += OnViewEnd;    

    }    

    private void OnViewStart()
    {
        closedEyesTime = 0f;
        inView = true;
        eyeTrackerCoroutine = StartCoroutine(EyeTrackerCoroutine());

        elapsedTime = 0f;
    }
    private void OnViewEnd()
    {
        inView = false;
        if (eyeTrackerCoroutine != null) StopCoroutine(eyeTrackerCoroutine);
    }

    private void LateUpdate()
    {
        elapsedTime += Time.deltaTime;

        ReadEyePosition();
    }

    IEnumerator EyeTrackerCoroutine()
    {
        while (true)
        {
            recordEye = true;
            yield return new WaitForSeconds(samplePeriod);
        }
    }

    private void ReadEyePosition()
    {
        // This function will Read eye Position; It will check if the softare is working, if the eye are detected and than it will calculate it
        // Final vector of eye direction is saved into Results"Name"All.txt, as vector x,y,z and time
        // Head rotation is saved in results"Name"Head.txt; in degrees of x,y,z and time
        // Only eye positions is saved in result"Name"eye.txt; in degrees of x,y and time
        //
        // In case that the software cannot detect the eyes for length of SkipTime the image will be skiped

        if (SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.WORKING) return;

        if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == true && eye_callback_registered == false)
        {
            SRanipal_Eye.WrapperRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye.CallbackBasic)EyeCallback));
            eye_callback_registered = true;
        }
        else if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == false && eye_callback_registered == true)
        {
            SRanipal_Eye.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye.CallbackBasic)EyeCallback));
            eye_callback_registered = false;
        }

        Vector3 eyeOrigin, eyeDir;


        head.position = VRcam.transform.position;
        head.rotation = VRcam.transform.localRotation;
        if (recordEye)
        {
            record.RaiseOnRecordHead(head.rotation.eulerAngles);
            record.RaiseOnRecordTime(elapsedTime);
        }

        if (eye_callback_registered)
        {
            if (SRanipal_Eye.GetGazeRay(GazeIndex.COMBINE, out eyeOrigin, out eyeDir, eyeData))
            {
                eyeDir = Rad2deg(eyeDir);
                
                
                //Vector2 newEye = new Vector2(eyeDir.x * Mathf.Cos(VrEuler.z) - eyeDir.y * Mathf.Sin(VrEuler.z), eyeDir.x * Mathf.Sin(VrEuler.z) - eyeDir.y * Mathf.Cos(VrEuler.z));
                

                eye.localRotation = Quaternion.Euler(-eyeDir.y, eyeDir.x, 0f);
                //transform.position = VRcam.transform.position;
                //transform.rotation = Quaternion.Euler(VrEuler.x + newEye.y, VrEuler.y + newEye.x, 0f);
                Vector3 eyeV = new Vector3(eyeDir.x, eyeDir.y, 0f);
                //events.RaiseOnGazeDirection(Vector3.Normalize(gaze.position - head.position + offset));
                //Quaternion mainRotation = VRcam.transform.rotation * eye.transform.rotation;
                events.RaiseOnGazeDirection(eyeV);

                
                if (recordEye)
                {
                    record.RaiseOnRecordEye(eyeDir);
                    record.RaiseOnRecordAll(Vector3.Normalize(gaze.position - head.position));

                    recordEye = false;
                }

                closedEyesTime = 0f;
            }
            else
            {
                if (recordEye)
                {
                    record.RaiseOnRecordNull();

                    recordEye = false;
                }

                closedEyesTime += Time.deltaTime;
                if (closedEyesTime >= skipTime && inView)
                {
                    // End View
                    Debug.Log("Ending View because of closed eyes");
                    events.RaiseOnViewEnd();
                }
            }

        }
    }

    public void EyeCalibration()
    {
        SRanipal_Eye_v2.LaunchEyeCalibration();
    }

    private void OnDisable()
    {
        Release();

        events.OnViewStart -= OnViewStart;
        events.OnViewEnd -= OnViewEnd;
    }

    void OnApplicationQuit()
    {
        Release();
    }

    /// <summary>
    /// Release callback thread when disabled or quit
    /// </summary>
    private static void Release()
    {
        if (eye_callback_registered == true)
        {
            SRanipal_Eye.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye.CallbackBasic)EyeCallback));
            eye_callback_registered = false;
        }
    }

    /// <summary>
    /// Required class for IL2CPP scripting backend support
    /// </summary>
    internal class MonoPInvokeCallbackAttribute : System.Attribute
    {
        public MonoPInvokeCallbackAttribute() { }
    }

    /// <summary>
    /// Eye tracking data callback thread.
    /// Reports data at ~120hz
    /// MonoPInvokeCallback attribute required for IL2CPP scripting backend
    /// </summary>
    /// <param name="eye_data">Reference to latest eye_data</param>
    [MonoPInvokeCallback]
    private static void EyeCallback(ref EyeData eye_data)
    {
        eyeData = eye_data;
        // do stuff with eyeData...
    }

    private static Vector3 Rad2deg(Vector3 v)
    {
        v.x = v.x * Mathf.Rad2Deg;
        v.z = v.z * Mathf.Rad2Deg;
        v.y = v.y * Mathf.Rad2Deg;

        return v;
    }
}
