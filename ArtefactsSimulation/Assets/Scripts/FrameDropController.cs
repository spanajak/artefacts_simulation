using System.Collections.Generic;
using UnityEngine;

public class FrameDropController : MonoBehaviour
{
    private Transform VrCamera;
    public int frameRepetition = 1;
    List<Vector3> rotationList;
    private int frameCounter =0;
    private Transform backgroundPivot;
    private Transform background;
    private Transform pivot2;
    private Transform pivot3;

    private bool on = false;

    private bool waiting = false;
    private float coolDown;
    private float coolDownTimer =0f;

    private bool dynamicFD;

    private float interval;
    private Vector2 intervalRange;

    private MainHandler handler;

    void Awake()
    {
        rotationList = new List<Vector3>();
        handler = GetComponent<MainHandler>();
    }

    private void Start()
    {
        VrCamera = handler.GetCamera();
        backgroundPivot = handler.GetBackground().GetPivot();
        background = handler.GetBackground().GetBackGround();
        pivot2 = handler.GetBackground().GetPivot2();
        pivot3 = handler.GetBackground().GetPivot3();
    }

    public void Activate()
    {
        SetCooldown();
        frameCounter = 0;
        on = false;
    }

    public void Deactivate()
    {
        backgroundPivot.position = Vector3.zero;
        backgroundPivot.localRotation = Quaternion.Euler(0f, 0f, 0f);

        background.localRotation = Quaternion.Euler(0f, 0f, 0f);
        pivot2.localRotation = Quaternion.Euler(0f, 0f, 0f);
        pivot3.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }

    public void LateDo()
    {
        backgroundPivot.position = VrCamera.position;
        
        if (on)
        {
            // set rotation
            background.position = VrCamera.position;
            backgroundPivot.rotation = VrCamera.localRotation;
            // count one frame
            frameCounter--;

            if (frameCounter <= 0)
            {
                // Switch state
                on = false;
                SetCooldown();
                

                backgroundPivot.localRotation = Quaternion.Euler(0f, 0f, 0f);
                background.localRotation = Quaternion.Euler(0f, 0f, 0f);
                pivot2.localRotation = Quaternion.Euler(0f, 0f, 0f);
                pivot3.localRotation = Quaternion.Euler(0f, 0f, 0f);
            }
        }
        else {


            // Timer
            coolDownTimer += Time.deltaTime;
            if (coolDownTimer > coolDown)
            {
                on = true;
                frameCounter = frameRepetition;
                coolDownTimer = 0f;

                // Set frame Drop location
                Vector3 newRotation = VrCamera.localEulerAngles;
                background.localRotation = Quaternion.Euler(0f, -newRotation.y, 0f);
                pivot2.localRotation = Quaternion.Euler(-newRotation.x, 0f, 0f);
                pivot3.localRotation = Quaternion.Euler(0f, 0f, -newRotation.z);
            }

        }

        /*
        if (rotationList.Count > 0)
        {
            //backgroundPivot.eulerAngles = new Vector3(-VrCamera.transform.eulerAngles.x, VrCamera.transform.eulerAngles.y, VrCamera.transform.eulerAngles.z) - rotationList[0];

            //backgroundPivot.localEulerAngles = new Vector3(-VrCamera.transform.localEulerAngles.x , VrCamera.transform.localEulerAngles.y - rotationList[0].y ,0f);
            background.position = VrCamera.position;
            backgroundPivot.rotation = VrCamera.localRotation;

            rotationList.RemoveAt(0);
        }
        else {

            backgroundPivot.rotation = Quaternion.identity;
            background.rotation = Quaternion.identity;
            pivot2.rotation = Quaternion.identity;
            pivot3.rotation = Quaternion.identity;

            if (waiting)
            {
                coolDownTimer += Time.deltaTime;
                if (coolDownTimer > coolDown)
                {
                    waiting = false;
                    coolDownTimer = 0f;
                }
                else {
                    return;
                }
            }

            
            Vector3 newRotation = VrCamera.localEulerAngles;
            

            background.rotation = VrCamera.rotation;
            for (int i = 0; i < frameRepetition; i++)
            {
                rotationList.Add(newRotation);
                //background.eulerAngles = new Vector3(-rotationList[0].x, -rotationList[0].y, -rotationList[0].z);

                background.localRotation = Quaternion.Euler(0f, -rotationList[0].y, 0f);
                pivot2.localRotation = Quaternion.Euler(-rotationList[0].x, 0f, 0f);
                pivot3.localRotation = Quaternion.Euler(0f, 0f, -rotationList[0].z);
            }

            SetCooldown();
        }*/
    }

    public void SetFrameRepetition(int newFR)
    {
        frameRepetition = newFR;
        rotationList.Clear();
    }

    public void SetInterval(float newInterval)
    {
        interval = newInterval;
        dynamicFD = false;
    }

    public void SetInterval(Vector2 newIntervalRange)
    {
        intervalRange = newIntervalRange;
        dynamicFD = true;
    }


    public void SetCooldown()
    {
        if (!dynamicFD)
        {
            coolDown = interval; 
        }
        else {
            coolDown = Random.Range(intervalRange.x, intervalRange.y);
        }
        waiting = true;
    }
}
