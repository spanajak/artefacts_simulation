using UnityEngine;
using System.Collections.Generic;

public class ViewManager : MonoBehaviour
{
    [SerializeField]
    [HideInInspector]
    public int currentViewIdx = -1;
    [HideInInspector]
    public int viewsCnt = 0;
    private TestEvents events;
    private RecordEvents record;

    private List<ActiveView> activeViews;
    [HideInInspector]
    public ActiveView currentActiveView;

    private bool viewing = false;

    private void Awake()
    {

        activeViews = new List<ActiveView>();

        

        List<BaseView> viewsList = new List<BaseView>(GetComponents<BaseView>());

        foreach (BaseView bv in viewsList)
        {
            List<int> activeElements = bv.Get();

            foreach (int ae in activeElements)
            {
                activeViews.Add(new ActiveView(bv, ae));
                
            }
        }

        viewsCnt = activeViews.Count;

        events = GetComponent<TestEvents>();
        record = GetComponent<RecordEvents>();

        events.OnStart += OnStart;
        events.OnViewStart += OnViewStart;
        events.OnViewEnd += OnViewEnd;
    }
    private void OnDisable()
    {
        events.OnStart -= OnStart;
        events.OnViewStart -= OnViewStart;
        events.OnViewEnd -= OnViewEnd;
    }

    private void OnStart()
    {
        Shuffle(activeViews);
        currentViewIdx = -1;
    }

    private void OnViewStart()
    {
        viewing = true;
    }

    private void OnViewEnd()
    {
        viewing = false;
    }

    public void DeactivateView()
    {
        currentActiveView.baseView.Deactivate();
        currentActiveView = null;
    }

    private void Update()
    {
        if (viewing && currentActiveView != null)
        {
            currentActiveView.baseView.Do();
        }
    }

    private void LateUpdate()
    {
        if (viewing && currentActiveView != null)
        {
            currentActiveView.baseView.LateDo();
        }
    }

    // return false if run out of views
    public bool NextView()
    {
        if (currentActiveView != null)
        {
            currentActiveView.baseView.Deactivate();
        }

        currentViewIdx++;

        if (currentViewIdx >= activeViews.Count)
        {
            Debug.Log("No longer any other artefacts");
            currentViewIdx = -1;
            return false;
        }


        currentActiveView = activeViews[currentViewIdx];
        currentActiveView.baseView.Set(currentActiveView.activeElement);
        currentActiveView.baseView.Activate();
        
        Debug.Log("current view is: " + currentActiveView.baseView.GetName());

        viewing = true;
        record.RaiseOnNewViewName(currentActiveView.baseView.GetName());

        return true;
    }

    void Shuffle<T>(List<T> list)
    {
        System.Random random = new System.Random();
        int n = list.Count;

        for (int i = n - 1; i > 0; i--)
        {
            int j = random.Next(0, i + 1);

            T temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    }
}

public class ActiveView
{
    public BaseView baseView;
    public int activeElement;

    public ActiveView(BaseView baseView, int activeElement)
    {
        this.baseView = baseView;
        this.activeElement = activeElement;
    }
}
