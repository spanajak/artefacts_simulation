using System.IO;
using System.Collections.Generic;
using System;
using MathNet.Numerics.LinearAlgebra;
using UnityEngine;

public class CVD : MonoBehaviour
{
    private List<float> r;
    private List<float> g;
    private List<float> b;

    private List<float> L;
    private List<float> M;
    private List<float> S;

    private List<float> WS;
    private List<float> YB;
    private List<float> RG;

    private float wsr, wsg, wsb, ybr, ybg, ybb, rgr, rgg, rgb;

    private float[,] result;

    private Matrix<float> normGamma;


    // Start is called before the first frame update
    void Start()
    {
        r = ReadTextFileToFloatList("Assets/TxtFiles/r.txt");
        g = ReadTextFileToFloatList("Assets/TxtFiles/g.txt");
        b = ReadTextFileToFloatList("Assets/TxtFiles/b.txt");

        L = ReadTextFileToFloatList("Assets/TxtFiles/L.txt");
        M = ReadTextFileToFloatList("Assets/TxtFiles/M.txt");
        S = ReadTextFileToFloatList("Assets/TxtFiles/S.txt");

        float[,] normGammaf = GetGamma(L, M, S, r, g, b);
        //List<float> Ma = ShiftList<float>(M, 19);
        //float[,] aGamma = GetGamma(L, Ma, S, r, g, b);

        //Print2DArray(normGamma);
        //Print2DArray(aGamma);

        normGamma = Matrix<float>.Build.DenseOfArray(normGammaf);
        /*Matrix<float> matrix2 = Matrix<float>.Build.DenseOfArray(aGamma);

        Matrix<float> X = normGamma.Solve(matrix2);
        X = X * 100f;
        */
        //Print2DArray(X.ToArray());

        GetRGBMatrix(-15, 19, 0);
    }

    private float[,] GetGamma(List<float> L, List<float> M, List<float> S, List<float> r, List<float> g, List<float> b)
    {
        List<List<float>> TM = new List<List<float>>
        {
            new List<float> { 0.600f, 0.400f, 0.000f },
            new List<float> { 0.240f, 0.105f, -0.700f },
            new List<float> { 1.200f, -1.600f, 0.400f }
        };

        List<List<float>> LMS = new List<List<float>>();
        LMS.Add(L);
        LMS.Add(M);
        LMS.Add(S);

        //Print2DList(LMS);
        List<List<float>> result = MultiplyMatrices(TM, LMS);
        WS = result[0];
        YB = result[1];
        RG = result[2];

        wsr = CalculateDotProduct(r, WS);
        wsg = CalculateDotProduct(g, WS);
        wsb = CalculateDotProduct(b, WS);

        ybr = CalculateDotProduct(r, YB);
        ybg = CalculateDotProduct(g, YB);
        ybb = CalculateDotProduct(b, YB);

        rgr = CalculateDotProduct(r, RG);
        rgg = CalculateDotProduct(g, RG);
        rgb = CalculateDotProduct(b, RG);

        float rho_WS = 1 / (wsr + wsg + wsb);
        float rho_YB = 1 / (ybr + ybg + ybb);
        float rho_RG = 1 / (rgr + rgg + rgb);

        float[,] gamma = new float[3, 3];

        gamma[0, 0] = rho_WS * wsr;
        gamma[0, 1] = rho_WS * wsg;
        gamma[0, 2] = rho_WS * wsb;

        gamma[1, 0] = rho_YB * ybr;
        gamma[1, 1] = rho_YB * ybg;
        gamma[1, 2] = rho_YB * ybb;

        gamma[2, 0] = rho_RG * rgr;
        gamma[2, 1] = rho_RG * rgg;
        gamma[2, 2] = rho_RG * rgb;

        return gamma;
    }

    
    public float[,] GetRGBMatrix(int Lshift, int Mshift, int Sshift)
    {
        List<float> La = ShiftList<float>(L, Lshift);
        List<float> Ma = ShiftList<float>(M, Mshift);
        List<float> Sa = ShiftList<float>(S, Sshift);

        float[,] aGamma = GetGamma(La, Ma, Sa, r, g, b);

        Matrix<float> aMatrix = Matrix<float>.Build.DenseOfArray(aGamma);

        Matrix<float> X = normGamma.Solve(aMatrix);
        X = X * 100f;

        //Print2DArray(X.ToArray());
        return X.ToArray();
    }
    
    static List<T> ShiftList<T>(List<T> list, int positions)
    {
        int count = list.Count;

        if (count == 0)
        {
            // Handle empty list
            return list;
        }

        positions = positions % count; // Ensure positions is within the range of the list

        if (positions == 0)
        {
            // No need to shift if positions is 0
            return list;
        }

        List<T> newList = new List<T>(list);

        if (positions > 0)
        {
            // Right shift (positive positions)
            List<T> shiftedPart = newList.GetRange(count - positions, positions);
            newList.RemoveRange(count - positions, positions);
            newList.InsertRange(0, shiftedPart);
        }
        else
        {
            // Left shift (negative positions)
            positions = Math.Abs(positions); // Convert negative positions to positive
            List<T> shiftedPart = newList.GetRange(0, positions);
            newList.RemoveRange(0, positions);
            newList.AddRange(shiftedPart);
        }

        return newList;
    }

    static List<List<float>> MultiplyMatrices(List<List<float>> matrixA, List<List<float>> matrixB)
    {
        int numRowsA = matrixA.Count;
        int numColsA = matrixA[0].Count;
        int numColsB = matrixB[0].Count;

        if (numColsA != matrixB.Count)
        {
            throw new InvalidOperationException("Matrix dimensions are not suitable for multiplication.");
        }

        List<List<float>> resultMatrix = new List<List<float>>();
        for (int i = 0; i < numRowsA; i++)
        {
            resultMatrix.Add(new List<float>());
            for (int j = 0; j < numColsB; j++)
            {
                float sum = 0.0f;
                for (int k = 0; k < numColsA; k++)
                {
                    sum += matrixA[i][k] * matrixB[k][j];
                }
                resultMatrix[i].Add(sum);
            }
        }

        return resultMatrix;
    }

    static float CalculateDotProduct(List<float> vectorA, List<float> vectorB)
    {
        if (vectorA.Count != vectorB.Count)
        {
            throw new InvalidOperationException("Vector dimensions do not match.");
        }

        float dotProduct = 0.0f;

        for (int i = 0; i < vectorA.Count; i++)
        {
            dotProduct += vectorA[i] * vectorB[i];
        }

        return dotProduct;
    }


    void Print2DList(List<List<float>> list)
    {
        string line = "";
        for (int i = 0; i < list[0].Count; i++)
        {
            for (int j = 0; j < list.Count; j++)
            {
                line += list[j][i].ToString() + " ";
               
            }
            Debug.Log(i.ToString() + ": "+line);
            line = "";
        }
    }

    static void Print2DArray(float[,] array)
    {
        int numRows = array.GetLength(0);
        int numCols = array.GetLength(1);
        string line = "";
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numCols; j++)
            {
                line += array[j,i].ToString() + " ";
            }
            Debug.Log(i.ToString() + ": " + line);
            line = "";
        }
    }

    static List<float> ReadTextFileToFloatList(string filePath)
    {
        List<float> values = new List<float>();

        try
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (float.TryParse(line, out float floatValue))
                    {
                        values.Add(floatValue);
                    }
                    else
                    {
                        Console.WriteLine("Failed to parse line as a float: " + line);
                    }
                }
            }
        }
        catch (IOException e)
        {
            Console.WriteLine("An error occurred while reading the file: " + e.Message);
        }

        return values;
    }
}
