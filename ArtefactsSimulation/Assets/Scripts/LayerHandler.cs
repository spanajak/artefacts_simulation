using UnityEngine;

public class LayerHandler : MonoBehaviour
{
    private MainHandler mainHandler;
    public Transform offset;
    public Transform pivot;

    private MaterialHandler matHandler;
    private GazeFollowLayer gazeFollowLayer;

    private MeshRenderer meshRenderer;

    private void Awake()
    {
        mainHandler = GetComponentInParent<MainHandler>();
        matHandler = GetComponent<MaterialHandler>();
        gazeFollowLayer = GetComponent<GazeFollowLayer>();

        meshRenderer = GetComponent<MeshRenderer>();

        Deactivate();
    }

    public void Activate()
    {
        meshRenderer.enabled = true;
    }

    public void Deactivate()
    {
        meshRenderer.enabled = false;
    }

    public void SetOffset(Vector3 offsetRot)
    {
        offset.rotation = Quaternion.Euler(offsetRot.x, offsetRot.y, offsetRot.z);
    }

    public GazeFollowLayer GetGazeFollow()
    {
        return gazeFollowLayer;
    }

    public Transform GetOffset()
    {
        return offset;
    }

    public Transform GetPivot()
    {
        return pivot;
    }

    public Transform GetCamera()
    {
        return mainHandler.GetCamera();
    }

    public MaterialHandler GetMatHandler()
    {
        return matHandler;
    }
}
