using UnityEngine;

public class FlipSpheres : MonoBehaviour
{

    private GameObject SferaPanoramica;

    public bool DoNotFlip;

    // Start is called before the first frame update
    void Start()
    {
        SferaPanoramica = this.transform.GetChild(0).gameObject;
        if (!DoNotFlip) InvertSphere();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) InvertSphere();
    }


    public void InvertSphere()
    {
        Vector3[] normals = SferaPanoramica.GetComponent<MeshFilter>().sharedMesh.normals;
        for (int i = 0; i < normals.Length; i++)
        {
            normals[i] = -normals[i];
        }
        SferaPanoramica.GetComponent<MeshFilter>().sharedMesh.normals = normals;

        int[] triangles = SferaPanoramica.GetComponent<MeshFilter>().sharedMesh.triangles;
        for (int i = 0; i < triangles.Length; i += 3)
        {
            int t = triangles[i];
            triangles[i] = triangles[i + 2];
            triangles[i + 2] = t;
        }

        SferaPanoramica.GetComponent<MeshFilter>().sharedMesh.triangles = triangles;
    }
}
